import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchoolsComponent } from './schools/schools.component';
import { SchoolFormComponent } from './school-form/school-form.component';

const routes: Routes = [
  { path: 'schools', component: SchoolsComponent },
  { path: 'school/:id', component: SchoolFormComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule { }
