import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { School } from './model/school';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  private schoolsUrl = 'api/schools';

  constructor(private http: HttpClient) { }

  getShools(): Observable<School[]> {
    return this.http.get<School[]>(this.schoolsUrl);
  }

  getSchool(id): Observable<School> {
    const url = `${this.schoolsUrl}/${id}`;
    return this.http.get<School>(url);
  }

  searchSchools(filter) : Observable<School[]> {
    return this.http.get<School[]>(this.schoolsUrl + "?filter=" + filter);
  }

  saveSchool(school): Observable<School> {
    return this.http.post<School>(this.schoolsUrl, school, httpOptions);    
  }

  updateSchool(school) : Observable<School> {
    const url = `${this.schoolsUrl}/${school.id}`;
    return this.http.put<School>(url, school, httpOptions);
  }

  deleteSchool(school): Observable<School> {
    const id = school.id;
    const url = `${this.schoolsUrl}/${id}`;

    return this.http.delete<School>(url, httpOptions);
  }
}
