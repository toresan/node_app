import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SchoolService } from '../school.service';
import { School } from '../model/school';

@Component({
  selector: 'app-school-form',
  templateUrl: './school-form.component.html',
  styleUrls: ['./school-form.component.css']
})
export class SchoolFormComponent implements OnInit {

  school: School;
  newSchool: boolean;

  constructor(private router: Router, private activeRouter: ActivatedRoute, private schoolService: SchoolService) { }

  ngOnInit() {
    this.loadSchool(+this.activeRouter.snapshot.paramMap.get('id'));
  }

  loadSchool(id): void {
    if (id > 0) {
      this.newSchool = false;
      this.schoolService.getSchool(id).subscribe(school => this.school = school);
    } else {
      this.newSchool = true;
      this.school = { id: 0, name: ""};
    }
  }

  onSubmit() {    
    if (this.newSchool) {
      this.schoolService.saveSchool(this.school).subscribe(school => this.school = school);
    } else {
      this.schoolService.updateSchool(this.school).subscribe(school => this.school = school);
    }
    this.router.navigateByUrl('/schools');
  }

}
