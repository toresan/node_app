import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

import * as $ from 'jquery';

import { School } from '../model/school';
import { SchoolService } from '../school.service';

@Component({
  selector: 'app-schools',
  templateUrl: './schools.component.html',
  styleUrls: ['./schools.component.css']
})
export class SchoolsComponent implements OnInit {

  schools: School[];
  searchField: FormControl = new FormControl();
  dataTable: any;

  constructor(private schoolService: SchoolService, private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getSchools();
    this.searchField
    .valueChanges
    .pipe(debounceTime(300))
    .subscribe(searchField => this.schoolService.searchSchools(searchField).subscribe(schools => this.schools = schools));
  }

  getSchools(): void {
    this.schoolService.getShools().subscribe(schools => {
      this.schools = schools;
      
      this.chRef.detectChanges();

      const table: any = $('table');
      this.dataTable = table.DataTable();
    });
  }

  deleteSchool(school : School): void {
    this.schools = this.schools.filter(h => h !== school);
    this.schoolService.deleteSchool(school).subscribe();
  } 

}
