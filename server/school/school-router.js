/**
 * Roteamento do caminho /api/school para chamadas do service.
 */
var express = require('express');
const SchoolService = require('./school-service');

var schoolRouter = express.Router();

schoolRouter
.get('/', async function(req, res) {
  res.status(200).send(await SchoolService.listSchools(req.query.filter));
})
.get('/:id', async function(req, res) {
  var school = await SchoolService.findSchool(req.params.id);
  if (school) {
    res.status(200).send(school);
  } else {
    res.status(404).send("School not found!");
  }
})
.post('/', async function(req, res) {
  var school = req.body;
  console.log(school);
  var created = await SchoolService.createSchool(school);
  res.status(201).send(created);
})
.put('/:id', async function(req, res) {
  var school = req.body;
  var updated = await SchoolService.updateSchool(req.params.id, school);
  if (updated) {
    res.status(200).send(school);
  } else {
    res.status(404).send("School not found!");
  }
})
.delete('/:id', async function(req, res) {
  var deleted = await SchoolService.deleteSchool(req.params.id);
  if (deleted) {
    res.status(200).send({ status: "Ok"} );
  } else {
    res.status(404).send("School not found!");
  }
});

module.exports = schoolRouter;