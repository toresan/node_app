/**
 * Serviço que tem a lógica de negócio, chama a biblioteca de acesso ao banco de dados quando necessário.
 */
const schoolDb = require('./school-db');
var SchoolService = function () {};

SchoolService.prototype.listSchools = async function(filter) {
  if (!filter) {
    return await schoolDb.list();
  }
  return await schoolDb.findByName(filter);
};

SchoolService.prototype.findSchool = async function(id) {
  var school = await schoolDb.find(id);
  if (school.length == 0) {
    return null;
  }
  return school[0];
};

SchoolService.prototype.createSchool = async function(school) {
  return await schoolDb.create(school);  
};

SchoolService.prototype.updateSchool = async function(id, school) {
  return await schoolDb.update(id, school);
};

SchoolService.prototype.deleteSchool = async function(id) {
  return await schoolDb.delete(id);
}

module.exports = new SchoolService();