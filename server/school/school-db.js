/**
 * Acessa a base de dados, mapeando os resultados para objetos.
 */

const joinjs = require('join-js').default;
const getConnection = require('../db/connectionPool');

const resultMap = [
  {
    mapId: 'schoolMap',
    idProperty: 'id',
    properties: ['name']
  }
]

const SchoolDb = function () { };

SchoolDb.prototype.list = async function () {
  client = await getConnection();
  try {
    const res = await client.query('SELECT s.id as school_id, s.name as school_name from school s order by s.name');
    let mappedResult = joinjs.map(res.rows, resultMap, 'schoolMap', 'school_');
    return mappedResult;
  } finally {
    client.release();
  }
};

SchoolDb.prototype.findByName = async function (name) {
  client = await getConnection();
  try {
    const params = [`%${name.toLowerCase()}%`]
    const res = await client.query("SELECT s.id as school_id, s.name as school_name from school s where lower(s.name) like $1 order by s.name", params);
    let mappedResult = joinjs.map(res.rows, resultMap, 'schoolMap', 'school_');
    return mappedResult;
  } finally {
    client.release();
  }
};

SchoolDb.prototype.find = async function (id) {
  client = await getConnection();
  try {
    const params = [id];
    const res = await client.query('SELECT s.id as school_id, s.name as school_name from school s where s.id = $1', params);

    let mappedResult = joinjs.map(res.rows, resultMap, 'schoolMap', 'school_');
    return mappedResult;
  } finally {
    client.release();
  }
};

SchoolDb.prototype.create = async function (school) {
  client = await getConnection();
  try {
    const params = Object.values(school);
    const res = await client.query('INSERT INTO school (id, name) VALUES ($1, $2)', params);
    return school;
  } finally {
    client.release();
  }
};

SchoolDb.prototype.update = async function (id, school) {
  client = await getConnection();
  try {
    const params = Object.values(school);
    params.push(id);
    const res = await client.query('UPDATE school set id = $1, name = $2 where id = $3', params);
    return res.rowCount;
  } finally {
    client.release();
  }
};

SchoolDb.prototype.delete = async function (id) {
  client = await getConnection();
  try {
    const params = [id];
    const res = await client.query('DELETE FROM school s where s.id = $1', params);
    return res.rowCount;
  } finally {
    client.release();
  }
}

module.exports = new SchoolDb();