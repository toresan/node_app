/*
  Exporta uma função que permite obter conexões com o banco de dados a partir de um pool.
  As configurações são lidas pela biblioteca 'config' do arquivo que está em config/default.json
*/
const config = require('config');
const { Pool } = require('pg')

const pool = new Pool({
  user: config.get('db.user'),
  host: config.get('db.host'),
  database: config.get('db.database'),
  password: config.get('db.password')
})

pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err)
  process.exit(-1)
})

module.exports = function() {
  return pool.connect();
};
