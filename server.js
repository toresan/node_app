/*
 Fonte principal do servidor, ponto de entrada da aplicação Node
*/
const express = require('express');
const path = require('path');
const http = require('http');
var bodyParser = require('body-parser');
const config = require('config');

const schoolRouter = require('./server/school/school-router');

// Cria uma aplicação ExpressJs, biblioteca para servidores Web
const app = express();

// Define que os arquivos estáticos serão servidos, a partir do diretório atual (__dirname), do diretório dist/school
app.use(express.static(path.join(__dirname, 'dist/school')));
// Adiciona um middleware para tratar os payloads JSON, transformando em objetos
app.use(bodyParser.json({ type: 'application/json' }));
// Mapeamento do caminho para o router
app.use('/api/schools', schoolRouter);

// Muda o padrão para o index.html
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/school/index.html'));
});

// Cria um servidor para a aplicação
const server = http.createServer(app);

// Começa a ouvir na porta 3000
server.listen(3000, () => console.log('Running server'));